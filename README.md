    WSGIScriptAlias / /home/apppick/public_html/django.wsgi
    WSGIDaemonProcess apppick user=apppick group=apppick  python-path=/home/unity3dindia/public_html:/home/apppick/public_html/myenv/lib/python2.6/site-packages
    WSGIProcessGroup  apppick
    WSGIPassAuthorization On

    Alias /media/ /home/apppick/public_html/mysite/media/
    Alias /static/ /home/apppick/public_html/mysite/static/


<Directory /home/apppick/public_html>
    <Files wsgi.py>
     Order deny,allow
     Allow from all
    </Files>
    </Directory>


    <Directory /home/apppick/public_html/mysite/static>
    Order deny,allow
    Allow from all
    </Directory>

    <Directory /home/apppick/public_html/mysite/media>
    Order deny,allow
    Allow from all
    </Directory>