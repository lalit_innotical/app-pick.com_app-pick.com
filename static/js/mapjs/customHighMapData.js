/**
 * Created by IDCS12 on 12/12/2014.
 */
$(function () {




    $(function () {
        var Alldata = [
            {
                "code": "IN",
                "value": 394,
                "name": "India"
            },
            {
                "code": "PK",
                "value": 225,
                "name": "Pakistan"
            },
            {
                "code": "LK",
                "value": 333,
                "name": "Sri Lanka"
            }

        ];

    CustomMap(Alldata);

    function CustomMap(data) {


        // Initiate the chart
        $('#geographicalmap').highcharts('Map', {
            chart : {
                borderWidth : 1
            },

            title : {
                text : 'Country Wise Downloads'
            },

            mapNavigation: {
                enabled: true
            },

            legend: {
                title: {
                    text: 'Downloads per Country'
                },
                align: 'left',
                verticalAlign: 'bottom',
                floating: true,
                layout: 'vertical',
                valueDecimals: 0,
                backgroundColor: 'rgba(255,255,255,0.9)',
                itemStyle: {
                    fontWeight: 'bold',
                    fontStyle: 'italic',
                    color: 'gray',
                    textDecoration: 'none'
                },
                itemHoverStyle: {
                    textDecoration: 'underline'
                }
            },

            colorAxis: {
                dataClasses: [{
                    to: 3,
                    color: 'rgba(19,64,117,0.05)'
                }, {
                    from: 3,
                    to: 10,
                    color: 'rgba(19,64,117,0.2)'
                }, {
                    from: 10,
                    to: 30,
                    color: 'rgba(19,64,117,0.4)'
                }, {
                    from: 30,
                    to: 100,
                    color: 'rgba(19,64,117,0.5)'
                }, {
                    from: 100,
                    to: 300,
                    color: 'rgba(19,64,117,0.6)'
                }, {
                    from: 300,
                    to: 1000,
                    color: 'rgba(19,64,117,0.8)'
                }, {
                    from: 1000,
                    color: 'rgba(19,64,117,1)'
                }]
            },

            series : [{
                data : data,
                mapData: Highcharts.maps['custom/world'],
                joinBy: ['iso-a2', 'code'],
                name: 'No. of Downloads',
                states: {
                    hover: {
                        color: '#BADA55'
                    }
                },
                tooltip: {
                    valueSuffix: ' Downloads'
                }
            }]
        });
    }
});
    });