/**
 * Created by IDCS12 on 12/15/2014.
 */
$(function () {
            var OwnData = [
                            {
                                name :'Spikxx',
                                y : 236
                            },
                            {
                                name :'Mr.Ducky',
                                y : 268
                            },
                            {
                                name : 'json',
                                y : 139
                            },
                            {
                                name: 'IdeaGellary',
                                y: 128,
                                sliced: true,
                                selected: true
                            },
                            {
                                name: 'PickApp',
                                y : 85
                            },
                            {
                                name : 'Temp',
                                y : 62
                            },
                            {
                                name : 'Tower',
                                y : 34
                            }


                        ]
            $(document).ready(function () {

                // Build the chart
                $('#PiYearWise').highcharts({
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false
                    },
                    title: {
                        text: 'AppDownload , 2014'
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: false
                            },
                            showInLegend: true
                        }
                    },
                    series: [{
                        type: 'pie',
                        name: 'Downloads',
                        data: OwnData
                    }]
                });
            });

        });