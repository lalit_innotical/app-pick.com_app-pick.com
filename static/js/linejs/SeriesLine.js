/**
 * Created by IDCS12 on 12/12/2014.
 */
$(function () {

    var seriesOptions = [],
        seriesCounter = 0,
        names = ['Spikxx', 'Mr.Ducky', 'PickApp'],
        // create the chart when all data is loaded
        createChart = function () {

            $('#MultiSeries').highcharts('StockChart', {

                rangeSelector: {
                    selected: 4
                },

                yAxis: {
                    labels: {
                        formatter: function () {
                            return (this.value > 0 ? ' + ' : '') + this.value + '%';
                        }
                    },
                    plotLines: [{
                        value: 0,
                        width: 2,
                        color: 'silver'
                    }]
                },

                plotOptions: {
                    series: {
                        compare: 'percent'
                    }
                },

                tooltip: {
                    pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.change}%)<br/>',
                    valueDecimals: 2
                },

                series: seriesOptions
            });
        };

    $.each(names, function (i, name) {
        var Owndata = ([[1417996800000,200],[1418083200000,439],[1418256000000,129],[1418342400000,216],[1418428800000,129]]);
        custom(Owndata)
        function custom (Owndata) {

            seriesOptions[i] = {
                name: name,
                data: Owndata
            };
            //alert(Owndata)

            // As we're loading the data asynchronously, we don't know what order it will arrive. So
            // we keep a counter and create the chart when all the data is loaded.
            seriesCounter += 1;

            if (seriesCounter === names.length) {
                createChart();
            }
        }
    });
});