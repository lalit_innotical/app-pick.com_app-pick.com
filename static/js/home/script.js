$(document).ready(function(){
   
    $(window).resize(function(){
        $('#hp').css({position:'absolute',top:($(window).height()-$('#hp').outerHeight())/2});
        $('#add').css({position:'absolute',top:($(window).height()-$('#add').outerHeight())/2});
        $('#feed').css({position:'absolute',top:($(window).height()-$('#feed').outerHeight())/2});
        $('#overview').css({position:'absolute',top:($(window).height()-$('#overview').outerHeight())/2});
    });
    $(window).resize();
    
    //AJAX
        
    function sendMail(inp){
        var regex = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
        if(regex.test(inp.val())){
            $.get("mail.php", { mail: inp.val()}).done(function(){
                $('div.formbefore').fadeOut(500);
                $('div.formafter').delay(500).fadeIn(500);
                inp.val('');
            });
        }else{
            alert('Email is not valid...');
        }
    }
    
    $('.newsletter input[type=button]').click(function(){
        var inp = $(this).parent('.newsletter').children('.newsletter input[type=text]');
        sendMail(inp);
    });
    
    $('.newsletter input[type=text]').keypress(function(e){
        if(e.which == 13){
            var inp = $(this);
            sendMail(inp);
        }
    });
    
    $('#top_video .button').click(function(){
        $('#top_video_obal').fadeIn(400);
    });
    
    $('#top_video_obal').click(function(){
        $('#top_video_obal').fadeOut(400);
        var iframe = $('#spendee_video')[0];
        var player = $f(iframe);
        player.api('pause');
    });
   
});