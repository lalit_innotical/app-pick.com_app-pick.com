from tastypie.resources import ModelResource,ALL
from rechargeapp.models import *
from tastypie.authentication import BasicAuthentication
from tastypie.authorization import Authorization
from django.contrib.auth.models import User
from tastypie import fields
from django.contrib.auth import authenticate, login, logout
from tastypie.http import HttpUnauthorized, HttpForbidden
from django.conf.urls import url
from tastypie.utils import trailing_slash
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from django.db import IntegrityError
from tastypie.exceptions import BadRequest
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.models import User
from tastypie.authorization import DjangoAuthorization
from tastypie.resources import *
from tastypie.authentication import BasicAuthentication, ApiKeyAuthentication, MultiAuthentication
from provider.oauth2.models import Client
import string
import random
from provider.oauth2.models import *




# Recharge Resource

class TestRechargeResource(ModelResource):
    user = fields.ForeignKey(UserResource,'user')

    class Meta:
        queryset = Recharge.objects.all()
        resource_name = 'testrecharge'
        filtering = {
            'user': ALL_WITH_RELATIONS,
            }
        #authorization = DjangoAuthorization()
        #authentication = OAuth20Authentication()



    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/makeRecharge%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('makeRecharge'), name="api_makeRecharge"),


        ]



    def makeRecharge(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        self.is_authenticated(request)

        try:

           data = self.deserialize(request, request.body, format = request.META.get('CONTENT_TYPE', 'application/json'))

           username = data.get('username', '')
           amount = data.get('amount', '')
           phoneNumber = data.get('phoneNumber', '')
           provider=data.get('provider','')


           print provider

           rechargeAmount=amount

           user=User.objects.get(username=username)

           userProfile=UserProfile.objects.get(user=user)

           wallet=Wallet.objects.get(user=userProfile)


           if wallet.coin <=9:
               return self.create_response(request, {"status":False,"massage":"User have at least 10 Rs."}, HttpUnauthorized )


           if wallet.coin-amount <0:
                return self.create_response(request, {"status":False,"massage":"User Have Negative Balance."}, HttpUnauthorized )


           recharge=Recharge.objects.create(user=user,rupee=amount,phoneNumber=phoneNumber,provider=provider)
           recharge.save()

           wallet.coin=wallet.coin-amount

           wallet.save()

           #email_subject = 'New Request for Recharge from Pic-app User'
           #email_body = "User Name : "+username+" \n Mobile No : "+phoneNumber+" \n Provider : "+provider+"\n Amount : "+amount
           #send_mail(email_subject, email_body, settings.EMAIL_HOST_USER,[userObj.email], fail_silently=False)




           return self.create_response(request, {"status":True,"massage":"Done"})





        except Exception,e:
           return self.create_response(request, {"status":False,"massage":e}, HttpUnauthorized )

