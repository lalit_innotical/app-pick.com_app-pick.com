# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Events.event_key'
        db.add_column(u'rechargeapp_events', 'event_key',
                      self.gf('django.db.models.fields.CharField')(default=0, unique=True, max_length=250),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Events.event_key'
        db.delete_column(u'rechargeapp_events', 'event_key')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'rechargeapp.app': {
            'Meta': {'object_name': 'App'},
            'company': ('django.db.models.fields.CharField', [], {'default': '0', 'max_length': '256'}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'icon': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'package': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'price': ('django.db.models.fields.IntegerField', [], {})
        },
        u'rechargeapp.appdownload': {
            'Meta': {'object_name': 'AppDownload'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['rechargeapp.App']"}),
            'countryCode': ('django.db.models.fields.CharField', [], {'max_length': '125', 'null': 'True', 'blank': 'True'}),
            'date': ('django.db.models.fields.DateField', [], {}),
            'deviceID': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'rechargeapp.apphistory': {
            'Meta': {'object_name': 'AppHistory'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['rechargeapp.App']"}),
            'countryCode': ('django.db.models.fields.CharField', [], {'max_length': '125', 'null': 'True', 'blank': 'True'}),
            'date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'totalDownload': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'visits': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'rechargeapp.appinstalled': {
            'Meta': {'unique_together': "(('user', 'app'),)", 'object_name': 'AppInstalled'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['rechargeapp.App']"}),
            'date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'redeemed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['rechargeapp.UserProfile']"})
        },
        u'rechargeapp.appvisits': {
            'Meta': {'object_name': 'AppVisits'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['rechargeapp.App']"}),
            'countryCode': ('django.db.models.fields.CharField', [], {'max_length': '125', 'null': 'True', 'blank': 'True'}),
            'date': ('django.db.models.fields.DateField', [], {}),
            'deviceID': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'rechargeapp.coins': {
            'Meta': {'object_name': 'Coins'},
            'coin': ('django.db.models.fields.IntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'rupee': ('django.db.models.fields.IntegerField', [], {})
        },
        u'rechargeapp.daily': {
            'Meta': {'object_name': 'Daily'},
            'date': ('django.db.models.fields.DateField', [], {}),
            'game': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['rechargeapp.Game']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'score': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        },
        u'rechargeapp.events': {
            'Meta': {'object_name': 'Events'},
            'coin': ('django.db.models.fields.IntegerField', [], {}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'event_key': ('django.db.models.fields.CharField', [], {'default': '0', 'unique': 'True', 'max_length': '250'}),
            'game': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['rechargeapp.Game']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'task': ('django.db.models.fields.CharField', [], {'max_length': '500'})
        },
        u'rechargeapp.game': {
            'Meta': {'object_name': 'Game'},
            'company': ('django.db.models.fields.CharField', [], {'default': '0', 'max_length': '256'}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'icon': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_monthly_price': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_weekly_price': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'monthly_first_price': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'monthly_second_price': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'monthly_third_price': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'package': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'price': ('django.db.models.fields.IntegerField', [], {}),
            'weekly_first_price': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'weekly_second_price': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'weekly_third_price': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'rechargeapp.gamedownload': {
            'Meta': {'object_name': 'GameDownload'},
            'countryCode': ('django.db.models.fields.CharField', [], {'max_length': '125', 'null': 'True', 'blank': 'True'}),
            'date': ('django.db.models.fields.DateField', [], {}),
            'deviceID': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'game': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['rechargeapp.Game']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'rechargeapp.gamehistory': {
            'Meta': {'object_name': 'GameHistory'},
            'countryCode': ('django.db.models.fields.CharField', [], {'max_length': '125', 'null': 'True', 'blank': 'True'}),
            'date': ('django.db.models.fields.DateField', [], {}),
            'game': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['rechargeapp.Game']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'totalDownload': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'visits': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'rechargeapp.gameinstalled': {
            'Meta': {'unique_together': "(('user', 'game'),)", 'object_name': 'GameInstalled'},
            'date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'game': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['rechargeapp.Game']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'redeemed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['rechargeapp.UserProfile']"})
        },
        u'rechargeapp.gamevisits': {
            'Meta': {'object_name': 'GameVisits'},
            'countryCode': ('django.db.models.fields.CharField', [], {'max_length': '125', 'null': 'True', 'blank': 'True'}),
            'date': ('django.db.models.fields.DateField', [], {}),
            'deviceID': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'game': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['rechargeapp.Game']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'rechargeapp.gift': {
            'Meta': {'object_name': 'Gift'},
            'coin': ('django.db.models.fields.IntegerField', [], {}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'icon': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'rechargeapp.giftredeem': {
            'Meta': {'object_name': 'GiftRedeem'},
            'date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'gift': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['rechargeapp.Gift']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['rechargeapp.UserProfile']"})
        },
        u'rechargeapp.iptrack': {
            'Meta': {'object_name': 'IPTrack'},
            'date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ipAdd': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'newdate': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'})
        },
        u'rechargeapp.iptrackone': {
            'Meta': {'object_name': 'IPTrackOne'},
            'date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ipAdd': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        },
        u'rechargeapp.iptracktwo': {
            'Meta': {'object_name': 'IPTrackTwo'},
            'date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ipAdd': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        },
        u'rechargeapp.leaderboard': {
            'Meta': {'unique_together': "(('user', 'game'),)", 'object_name': 'LeaderBoard'},
            'game': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['rechargeapp.Game']"}),
            'highScore': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'npt': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        },
        u'rechargeapp.monthly': {
            'Meta': {'object_name': 'Monthly'},
            'date': ('django.db.models.fields.DateField', [], {}),
            'game': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['rechargeapp.Game']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'monthNo': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'score': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"}),
            'year': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'rechargeapp.recharge': {
            'FAILED': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'Meta': {'object_name': 'Recharge'},
            'PENDING': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'SUCCESS': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_cron': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'orderID': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'phoneNumber': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'provider': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'rupee': ('django.db.models.fields.IntegerField', [], {}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']", 'null': 'True', 'blank': 'True'})
        },
        u'rechargeapp.refer': {
            'Meta': {'object_name': 'Refer'},
            'date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'from_user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'from_user'", 'to': u"orm['auth.User']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'to_user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'to_user'", 'to': u"orm['auth.User']"})
        },
        u'rechargeapp.userprofile': {
            'Meta': {'object_name': 'UserProfile'},
            'country': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'deviceID': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'gcmID': ('django.db.models.fields.CharField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'phoneNumber': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'referBy': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'refer_user'", 'null': 'True', 'to': u"orm['auth.User']"}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'main_user'", 'unique': 'True', 'to': u"orm['auth.User']"})
        },
        u'rechargeapp.wallet': {
            'Meta': {'object_name': 'Wallet'},
            'coin': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'redeemed': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['rechargeapp.UserProfile']"})
        },
        u'rechargeapp.weekly': {
            'Meta': {'object_name': 'Weekly'},
            'date': ('django.db.models.fields.DateField', [], {}),
            'game': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['rechargeapp.Game']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'month': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'score': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"}),
            'weekNo': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'year': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        }
    }

    complete_apps = ['rechargeapp']