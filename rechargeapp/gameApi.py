from tastypie.resources import ModelResource,ALL
from rechargeapp.models import *
from tastypie.authentication import BasicAuthentication
from tastypie.authorization import Authorization
from django.contrib.auth.models import User
from tastypie import fields
from django.contrib.auth import authenticate, login, logout
from tastypie.http import HttpUnauthorized, HttpForbidden
from django.conf.urls import url
from tastypie.utils import trailing_slash
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from django.db import IntegrityError
from tastypie.exceptions import BadRequest
from django.http import HttpResponseRedirect, HttpResponse
from provider.oauth2.models import Client
from authentication import OAuth20Authentication
from tastypie.authorization import DjangoAuthorization
from tastypie.resources import *
import string
import random
from Crypto.Cipher import AES
import binascii
from django.core.mail import send_mail
from provider.oauth2.models import *
import urllib3


from rechargeapp.api import *

class GameSDKResource(ModelResource):

    class Meta:
        queryset = User.objects.all()
        resource_name = 'gamesdk'

        authorization = DjangoAuthorization()
        authentication = OAuth20Authentication()
        

    def prepend_urls(self):
        return [

            url(r"^(?P<resource_name>%s)/updatescore%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('updatescore'), name="api_updatescore"),
                
            url(r"^(?P<resource_name>%s)/checkGameStatus%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('checkGameStatus'), name="api_checkGameStatus"),
                
                
                

        ]

    def updatescore(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        self.is_authenticated(request)
        data = self.deserialize(request, request.body, format = request.META.get('CONTENT_TYPE', 'application/json'))

        try:

            username = data.get('username', '')
            package = data.get('package', '')
            currentScore=data.get('score', '')

            #print currentScore

            user=User.objects.get(username=username)
            userProfile=UserProfile.objects.get(user=user)
            game=Game.objects.get(package=package)

            date=datetime.now().date()
            weekNo=date.isocalendar()[1]
            monthNo=date.timetuple()[1]


            leaderObj=LeaderBoard.objects.get(user=user,game=game)

            if int(currentScore) > int(leaderObj.highScore):
                leaderObj.highScore=int(currentScore)
                leaderObj.npt=leaderObj.npt+1
                leaderObj.save()

            else:
                leaderObj.npt=leaderObj.npt+1
                leaderObj.save()

#Daily

            if Daily.objects.filter(user=user,game=game,date=date).exists():
                #print "yes"

                daily=Daily.objects.get(user=user,game=game,date=date)

                if int(currentScore) > int(daily.score):
                    daily.score=int(currentScore)
                    daily.save()


            else:
                daily=Daily.objects.create(user=user,game=game,score=int(currentScore),date=date)
                daily.save()


#Weekly

            if Weekly.objects.filter(user=user,game=game,weekNo=weekNo,year=date.year,month=date.month).exists():
                print "yes"

                weekly=Weekly.objects.get(user=user,game=game,weekNo=weekNo,year=date.year,month=date.month)

                if int(currentScore) > int(weekly.score):
                    weekly.score=int(currentScore)
                    weekly.save()


            else:
                weekly=Weekly.objects.create(user=user,game=game,score=int(currentScore),weekNo=weekNo,date=date,year=date.year,month=date.month)
                daily.save()





#Monthly

            if Monthly.objects.filter(user=user,game=game,monthNo=monthNo,year=date.year).exists():
                #print "yes"

                monthly=Monthly.objects.get(user=user,game=game,monthNo=monthNo,year=date.year)

                if int(currentScore) > int(monthly.score):
                    monthly.score=int(currentScore)
                    monthly.save()


            else:
                monthly=Monthly.objects.create(user=user,game=game,score=int(currentScore),monthNo=monthNo,date=date,year=date.year)
                daily.save()




            return self.create_response(request, {'status':True,'message':None})

        except Exception,e:
            return self.create_response(request, {'status':False,'message':e},HttpUnauthorized)









    def checkGameStatus(self,request,**kwargs):

        self.method_check(request, allowed=['post'])
        self.is_authenticated(request)

        data = self.deserialize(request, request.body, format = request.META.get('CONTENT_TYPE', 'application/json'))


        try:


            username = data.get('username', '')
            package = data.get('package', '')



            game=Game.objects.get(package=package)
            user=User.objects.get(username=username)
            userProfile=UserProfile.objects.get(user=user)

            if GameInstalled.objects.filter(user=userProfile,game=game).exists():

                return self.create_response(request, {'success': True,'reason': "Game Already Install"})
            else:
                gameInstall=GameInstalled.objects.create(user=userProfile,game=game,redeemed=True)
                gameInstall.save()
                leaderBoard=LeaderBoard.objects.create(user=user,game=game)
                leaderBoard.save()

                return self.create_response(request, {'success': True,'reason': "Make new Game Entry"})



        except Exception,e:
               return self.create_response(request, {'success': False,'reason': e},HttpUnauthorized)




