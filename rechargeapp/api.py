from tastypie.resources import ModelResource,ALL
from rechargeapp.models import *
from tastypie.authentication import BasicAuthentication
from tastypie.authorization import Authorization
from django.contrib.auth.models import User
from tastypie import fields
from django.contrib.auth import authenticate, login, logout
from tastypie.http import HttpUnauthorized, HttpForbidden
from django.conf.urls import url
from tastypie.utils import trailing_slash
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from django.db import IntegrityError
from tastypie.exceptions import BadRequest
from django.http import HttpResponseRedirect, HttpResponse
from provider.oauth2.models import Client
from authentication import OAuth20Authentication
from tastypie.authorization import DjangoAuthorization
from tastypie.resources import *
import string
import random
from Crypto.Cipher import AES
import binascii
from django.core.mail import send_mail
from provider.oauth2.models import *
import urllib3
from django.conf import settings
import re
import requests
import json
from datetime import *
from gcm import *
from mysite.config import *
from django.db.models import Q
from tastypie.authentication import Authentication











#get a random password
 
def password_generator_upper(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))







def sendGCM(title,message,id):
    try:    
        gcm = GCM(settings.GCM_ID)
        data = {'title': title,'message':message}
        gcm.plaintext_request(registration_id=id, data=data)
    except Exception, e:
        print e           



# User Model for password and username

class UserResource(ModelResource):

    class Meta:
        queryset = User.objects.all()
        resource_name = 'user'
        
        authorization = DjangoAuthorization()
        authentication = OAuth20Authentication()     
        #always_return_data = True 
        
        excludes = ['password', 'is_active', 'is_staff', 'is_superuser','date_joined','last_login','id']
        list_allowed_methods = ['post']

        filtering = {
            'username': ALL,
            'id' : ALL,
         }
  



    def obj_create(self, bundle, request=None, **kwargs):
        try:
            bundle = super(UserResource, self).obj_create(bundle, **kwargs)
            bundle.obj.save()
        except IntegrityError:
            raise BadRequest('That username already exists')
        return bundle


#for Log in

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/login%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('login'), name="api_login"),
            url(r'^(?P<resource_name>%s)/logout%s$' %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('logout'), name='api_logout'),
                
                
            url(r"^(?P<resource_name>%s)/changePassword%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('changePassword'), name="api_changePassword"),
    
        ]



    def changePassword(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        self.is_authenticated(request)

        data = self.deserialize(request, request.body, format = request.META.get('CONTENT_TYPE', 'application/json'))


        try:


            username = data.get('username', '')
            oldPassword= data.get('oldPassword', '')
            newPassword = data.get('newPassword', '')


            userObj=User.objects.get(username=username)
            if userObj.check_password(oldPassword):

                userObj.set_password(newPassword)
                userObj.save()

                return self.create_response(request, {'success': True,'reason': 'Done'})
            else:
                return self.create_response(request, {'success': False,'reason':'000'},HttpUnauthorized)
                
#  000 -> please enter a correct password 

        except Exception,e:

            return self.create_response(request, {'success': False,'reason': "Error"}, HttpUnauthorized )






#for User Profile   mobile no ,device id, country

class EntryResource(ModelResource):
    user = fields.ToOneField(UserResource, 'user',full=True)
    referBy = fields.ForeignKey(UserResource,'referBy',full=True,null=True)   
    class Meta:
        queryset = UserProfile.objects.all()
        resource_name = 'entry'
        #authorization= Authorization()

        always_return_data = True 
        
        filtering = {
            'user': ALL_WITH_RELATIONS,'referBy': ALL_WITH_RELATIONS,  
            }
     
        excludes = ['country','deviceID','id','gcmID'] 
        
        list_allowed_methods = ['get']           
            
        authorization = DjangoAuthorization()
        authentication = OAuth20Authentication()     


        
        

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/update%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('update'), name="api_update"),
                
            

        ]


    def update(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        self.is_authenticated(request)       

        data = self.deserialize(request, request.body, format = request.META.get('CONTENT_TYPE', 'application/json'))

        try:
            username = data.get('username', '')
            first_name = data.get('first_name', '')
            last_name = data.get('last_name', '')


            email = data.get('email', '')
            #country = data.get('country', '')
            phoneNumber=data.get('phoneNumber','')

            user=User.objects.get(username=username)


            if User.objects.filter(email=email).exclude(email=user.email).exists():
            	return self.create_response(request, {'success': False,'reason':"001"}, HttpUnauthorized )
#  "001" Email must Be unique


            user=User.objects.get(username=username)
            user.first_name=first_name
            user.last_name=last_name
            user.email=email
            user.save()

            userProfile=UserProfile.objects.get(user=user)
            userProfile.phoneNumber=phoneNumber
            #userProfile.country=country
            userProfile.save()


            return self.create_response(request, {'success': True,'reason': None})

        except Exception,e:
            return self.create_response(request, {'success': False,'reason': e}, HttpUnauthorized )
            
            
            
        
        
        
                   


# Getting All game list 

class GameResource(ModelResource):
    class Meta:
        queryset = Game.objects.all()        
        authorization = DjangoAuthorization()
        authentication = OAuth20Authentication()        
        resource_name = 'game'     
        always_return_data = True 
        filtering={'name':ALL}
        list_allowed_methods = ['get']         
        
        
# Getting all App list
class AppResource(ModelResource):

    class Meta:
        q1 = AppInstalled.objects.filter()
        queryset = App.objects.all()
        resource_name = 'app'
        authorization = DjangoAuthorization()
        authentication = OAuth20Authentication()        
        list_allowed_methods = ['get']       
  

# Getting list of installed app 

class AppInstalledResource(ModelResource):
    user = fields.ForeignKey(EntryResource,'user')
    app  = fields.ForeignKey(AppResource,'app')
    class Meta:
        queryset = AppInstalled.objects.all()
        resource_name = 'installation'
        
        authorization = DjangoAuthorization()
        authentication = OAuth20Authentication()        

        always_return_data = True 
        list_allowed_methods = ['get']          
        filtering = {
            'user': ALL_WITH_RELATIONS,
            }


    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/list%s$" %
                (self._meta.resource_name, trailing_slash()),self.wrap_view('list'), name="api_list"),
                           
            url(r"^(?P<resource_name>%s)/newEntry%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('newEntry'), name="newEntry"),
     

        ]



    def list(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        self.is_authenticated(request)    
        
   
        data = self.deserialize(request, request.body, format = request.META.get('CONTENT_TYPE', 'application/json'))

        username = data.get('username', '')

        user=User.objects.get(username=username)
        upObj=UserProfile.objects.get(user=user)

        result=[]


        obj=AppInstalled.objects.filter(user=upObj).exists()


        if not obj:

            d=App.objects.filter(is_active=True)

            for i in d:
                l={}
                l['name']=i.name
                l['packages']=i.package
                l['icon']=i.icon
                l['id']=i.id
                l['installedId']=-1
                l['price']=i.price
                l['company']=i.company                
                l['description']=i.description
                l['status']=True
                l['is_offer']=i.is_offer              
                l['offer_id']=i.offer_id                
                l['url']=i.url               
                
                result.append(l)

            return self.create_response(request, {"data":result})



        applist=[]
        appInstalledlist=[]
        redeemedAppList=[]



        for a in App.objects.filter(is_active=True):
            applist.append(a.name.encode('ascii','ignore'))


        for a in AppInstalled.objects.filter(user=upObj,redeemed=True):
            appInstalledlist.append(a.app.name.encode('ascii','ignore'))


        resultListRemind=list(set(applist)-set(appInstalledlist))


# get redeemed list

        for a in AppInstalled.objects.filter(user=upObj,redeemed=False):
            redeemedAppList.append(a.app.name.encode('ascii','ignore'))


        for obj in redeemedAppList:
            app={}

            a=App.objects.get(name=obj,is_active=True)
            app['name']=a.name
            app['packages']=a.package
            app['icon']=a.icon
            app['id']=a.id
            app['installedId']=-1
            app['installedId']=AppInstalled.objects.get(user=upObj,app=App.objects.get(name=obj)).id
            app['price']=a.price
            app['company']=a.company              
            app['description']=a.description
            app['status']=False
            app['is_offer']=a.is_offer              
            app['offer_id']=a.offer_id                
            app['url']=a.url             

            result.append(app)


# app witch not installed


        notInstallList=list(set(resultListRemind)-set(redeemedAppList))


        for obj in notInstallList:
            app={}

            a=App.objects.get(name=obj,is_active=True)
            app['name']=a.name
            app['packages']=a.package
            app['icon']=a.icon
            app['id']=a.id
            app['installedId']=-1
            app['price']=a.price
            app['company']=a.company              
            app['description']=a.description
            app['status']=True
            app['is_offer']=a.is_offer              
            app['offer_id']=a.offer_id                
            app['url']=a.url 
            result.append(app)



        return self.create_response(request,{"data":result})
 
 
 
 

    def newEntry(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        self.is_authenticated(request)

        try:


           data = self.deserialize(request, request.body, format = request.META.get('CONTENT_TYPE', 'application/json'))

           username = data.get('username', '')
           package = data.get('package', '')
           countryCode = data.get('countryCode', '')
           deviceID = data.get('deviceID', '')           

           user=User.objects.get(username=username)
           userProfile=UserProfile.objects.get(user=user)



           #checking for app

           if App.objects.filter(package=package).exists():

               print("app exist ")

               app=App.objects.get(package=package)
               
#               
               aa=AppDownload.objects.create(app=app,date=datetime.now().date(),countryCode=countryCode,deviceID=deviceID)
               aa.save()
                  
      
                                                  
#               
               
                             

               if AppInstalled.objects.filter(user=userProfile,app=app).exists():

                   return self.create_response(request, {"status":False,"massage":"User have already instal this app"}, HttpUnauthorized )

               else:

                    appInstalled=AppInstalled.objects.create(user=userProfile,app=app,redeemed=True)
                    appInstalled.save()


                    wallet=Wallet.objects.get(user=userProfile)
                    wallet.coin+=app.price
                    wallet.save()

                    return self.create_response(request, {"status":True,"massage":"App Successful"})


           #checking for game
           if Game.objects.filter(package=package).exists():

               #print("app exist ")
               
               
               

               game=Game.objects.get(package=package)
               
#

               gg=GameDownload.objects.create(game=game,date=datetime.now().date(),countryCode=countryCode,deviceID=deviceID)
               gg.save()
               
#               
                            

               if GameInstalled.objects.filter(user=userProfile,game=game).exists():

                   return self.create_response(request, {"status":False,"massage":"User have already install this game"}, HttpUnauthorized )

               else:

                    gameInstalled=GameInstalled.objects.create(user=userProfile,game=game,redeemed=True)
                    gameInstalled.save()



                    leaderBoard=LeaderBoard.objects.create(user=user,game=game,highScore=0,npt=0)
                    leaderBoard.save()

                    if app.is_offer == False :
                        wallet=Wallet.objects.get(user=userProfile)
                        wallet.coin+=game.price
                        wallet.save()

                    return self.create_response(request, {"status":True,"massage":"Game Successful"})

           else:
               return self.create_response(request, {"status":False,"massage":"000"}, HttpUnauthorized )
#000->>> package not found

        except Exception,e:

            return self.create_response(request, {"status":False,"massage":e}, HttpUnauthorized )

        



# Getting list of installed list


class GameInstalledResource(ModelResource):
    user = fields.ForeignKey(EntryResource,'user')
    game  = fields.ForeignKey(GameResource,'game',full=True)
    class Meta:
        queryset = GameInstalled.objects.all()
        resource_name = 'gameInstallation'
        
        authorization = DjangoAuthorization()
        authentication = OAuth20Authentication()        
        list_allowed_methods = ['get']       

        always_return_data = True 
        filtering = {
            'user': ALL_WITH_RELATIONS,
            }


    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/list%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('list'), name="api_login"),

        ]



    def list(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        self.is_authenticated(request)
        data = self.deserialize(request, request.body, format = request.META.get('CONTENT_TYPE', 'application/json'))

        username = data.get('username', '')

        user=User.objects.get(username=username)
        upObj=UserProfile.objects.get(user=user)

        result=[]


        obj=GameInstalled.objects.filter(user=upObj).exists()


        if not obj:

            d=Game.objects.filter(is_active=True)

            for i in d:
                l={}
                l['name']=i.name
                l['packages']=i.package
                l['icon']=i.icon
                l['id']=i.id
                l['installedId']=-1
                l['price']=i.price
                l['company']=i.company                  
                l['description']=i.description
                l['status']=True
                result.append(l)

            return self.create_response(request, {"data":result})



        applist=[]
        appInstalledlist=[]
        redeemedAppList=[]



        for a in Game.objects.filter(is_active=True):
            applist.append(a.name.encode('ascii','ignore'))


        for a in GameInstalled.objects.filter(user=upObj,redeemed=True):
            appInstalledlist.append(a.game.name.encode('ascii','ignore'))


        resultListRemind=list(set(applist)-set(appInstalledlist))


# get redeemed list

        for a in GameInstalled.objects.filter(user=upObj,redeemed=False):
            redeemedAppList.append(a.game.name.encode('ascii','ignore'))


        for obj in redeemedAppList:
            app={}

            a=Game.objects.get(name=obj,is_active=True)
            app['name']=a.name
            app['packages']=a.package
            app['icon']=a.icon
            app['id']=a.id
            app['installedId']=-1
            app['installedId']=GameInstalled.objects.get(user=upObj,game=Game.objects.get(name=obj)).id
            app['price']=a.price
            app['company']=a.company              
            app['description']=a.description
            app['status']=False

            result.append(app)


# app witch not installed


        notInstallList=list(set(resultListRemind)-set(redeemedAppList))


        for obj in notInstallList:
            app={}

            a=Game.objects.get(name=obj,is_active=True)
            app['name']=a.name
            app['packages']=a.package
            app['icon']=a.icon
            app['id']=a.id
            app['installedId']=-1
            app['price']=a.price
            app['company']=a.company             
            app['description']=a.description
            app['status']=True

            result.append(app)



        return self.create_response(request, {"data":result})






# gEt wallet information 

class WalletResource(ModelResource):
    user = fields.ForeignKey(EntryResource,'user')

    class Meta:
        queryset = Wallet.objects.all()
        resource_name = 'wallet'
        filtering = {
            'user': ALL_WITH_RELATIONS,
            }
        authorization = DjangoAuthorization()
        authentication = OAuth20Authentication()            
        list_allowed_methods = ['get']           
            
            
        
 
#  getting Score of user in game        
        
class LeaderBoardResource(ModelResource):
    user = fields.ForeignKey(UserResource,'user') 
    game = fields.ForeignKey(GameResource,'game')    

    class Meta:
         
        queryset = LeaderBoard.objects.all()
        resource_name = 'leaderBoard'
        filtering = {'game': ALL_WITH_RELATIONS,'user': ALL_WITH_RELATIONS}
        
        
        authorization = DjangoAuthorization()
        authentication = OAuth20Authentication()
        list_allowed_methods = ['list']



        
        
# getting top score in game   
     
     
class GameHighScoreResource(ModelResource):
    user = fields.ForeignKey(UserResource,'user',full=True) 
    game = fields.ForeignKey(GameResource,'game',full=True)    

    class Meta:
        queryset = LeaderBoard.objects.all()
        resource_name = 'gameScore'
        filtering = {'game': ALL_WITH_RELATIONS,'user': ALL_WITH_RELATIONS,}
        
        authorization = DjangoAuthorization()
        authentication = OAuth20Authentication()        
        list_allowed_methods = ['get']        



    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/allTimeScoreForApp%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('allTimeScoreForApp'), name="api_allTimeScoreForApp"),


            url(r"^(?P<resource_name>%s)/dailyScoreForApp%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('dailyScoreForApp'), name="api_dailyScoreForApp"),


            url(r"^(?P<resource_name>%s)/weeklyScoreForApp%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('weeklyScoreForApp'), name="api_weeklyScoreForApp"),


            url(r"^(?P<resource_name>%s)/monthlyScoreForApp%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('monthlyScoreForApp'), name="api_monthlyScoreForApp"),


        ]


    def allTimeScoreForApp(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        self.is_authenticated(request)
        data = self.deserialize(request, request.body, format = request.META.get('CONTENT_TYPE', 'application/json'))

        try:

            username = data.get('username', '')
            package = data.get('package', '')


            user=User.objects.get(username=username)
            #userProfile=UserProfile.objects.get(user=user)
            game=Game.objects.get(package=package)

            ldb=LeaderBoard.objects.filter(game=game).order_by('-highScore')[:10]

            topScore=[]
            userScore=[]

            for i,obj in enumerate(ldb):
                data={}
                data['first_name']=obj.user.first_name
                data['last_name']=obj.user.last_name
                data['score']=obj.highScore
                data['position']=i+1
                topScore.append(data)

            uldb=LeaderBoard.objects.filter(game=game).order_by('-highScore')


            for i, p in enumerate(uldb):
                if p.user.username == username :
                   print(i+1)
                   r={}
                   r['user_first_name']=p.user.first_name
                   r['user_last_name']=p.user.last_name
                   r['user_score']=p.highScore
                   r['user_position']=i+1
                   userScore.append(r)
                   break




            return self.create_response(request, {'userScore':userScore,'topScore':topScore,'status':True,'message':None})

        except Exception,e:
            return self.create_response(request, {'status':False,'message':e})

    def dailyScoreForApp(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        self.is_authenticated(request)
        data = self.deserialize(request, request.body, format = request.META.get('CONTENT_TYPE', 'application/json'))

        try:

            username = data.get('username', '')
            package = data.get('package', '')

            date=datetime.now().date()



            user=User.objects.get(username=username)
            #userProfile=UserProfile.objects.get(user=user)
            game=Game.objects.get(package=package)

            daily=Daily.objects.filter(game=game,date=date).order_by('-score')[:10]

            topScore=[]
            userScore=[]

            for i,obj in enumerate(daily):
                data={}
                data['first_name']=obj.user.first_name
                data['last_name']=obj.user.last_name
                data['score']=obj.score
                data['position']=i+1
                topScore.append(data)

            uDaily=Daily.objects.filter(game=game,date=date).order_by('-score')


            for i, p in enumerate(uDaily):
                if p.user.username == username :
                   print(i+1)
                   r={}
                   r['user_first_name']=p.user.first_name
                   r['user_last_name']=p.user.last_name
                   r['user_score']=p.score
                   r['user_position']=i+1
                   userScore.append(r)
                   break




            return self.create_response(request, {'userScore':userScore,'topScore':topScore,'status':True,'message':None})

        except Exception,e:
            return self.create_response(request, {'status':False,'message':e})

    def weeklyScoreForApp(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        self.is_authenticated(request)
        data = self.deserialize(request, request.body, format = request.META.get('CONTENT_TYPE', 'application/json'))

        try:

            username = data.get('username', '')
            package = data.get('package', '')

            date=datetime.now().date()
            weekNo=date.isocalendar()[1]


            user=User.objects.get(username=username)
            #userProfile=UserProfile.objects.get(user=user)
            game=Game.objects.get(package=package)

            weekly=Weekly.objects.filter(game=game,year=date.year,month=date.month,weekNo=weekNo).order_by('-score')[:10]

            topScore=[]
            userScore=[]

            for i,obj in enumerate(weekly):
                data={}
                data['first_name']=obj.user.first_name
                data['last_name']=obj.user.last_name
                data['score']=obj.score
                data['position']=i+1
                topScore.append(data)

            uWeekly=Weekly.objects.filter(game=game,year=date.year,month=date.month,weekNo=weekNo).order_by('-score')


            for i, p in enumerate(uWeekly):
                if p.user.username == username :
                   print(i+1)
                   r={}
                   r['user_first_name']=p.user.first_name
                   r['user_last_name']=p.user.last_name
                   r['user_score']=p.score
                   r['user_position']=i+1
                   userScore.append(r)
                   break




            return self.create_response(request, {'userScore':userScore,'topScore':topScore,'status':True,'message':None})

        except Exception,e:
            return self.create_response(request, {'status':False,'message':e})


    def monthlyScoreForApp(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        self.is_authenticated(request)
        data = self.deserialize(request, request.body, format = request.META.get('CONTENT_TYPE', 'application/json'))

        try:

            username = data.get('username', '')
            package = data.get('package', '')

            date=datetime.now().date()
            monthNo=date.timetuple()[1]


            user=User.objects.get(username=username)
            #userProfile=UserProfile.objects.get(user=user)
            game=Game.objects.get(package=package)

            monthly=Monthly.objects.filter(game=game,year=date.year,monthNo=monthNo).order_by('-score')[:5]

            topScore=[]
            userScore=[]

            for i,obj in enumerate(monthly):
                data={}
                data['first_name']=obj.user.first_name
                data['last_name']=obj.user.last_name
                data['score']=obj.score
                data['position']=i+1
                topScore.append(data)

            uMonthly=Monthly.objects.filter(game=game,year=date.year,monthNo=monthNo).order_by('-score')


            for i, p in enumerate(uMonthly):
                if p.user.username == username :
                   print(i+1)
                   r={}
                   r['user_first_name']=p.user.first_name
                   r['user_last_name']=p.user.last_name
                   r['user_score']=p.score
                   r['user_position']=i+1
                   userScore.append(r)
                   break




            return self.create_response(request, {'userScore':userScore,'topScore':topScore,'status':True,'message':None})

        except Exception,e:
            return self.create_response(request, {'status':False,'message':e})














 
#App History 


class AppHistoryResource(ModelResource):
    user = fields.ForeignKey(EntryResource,'user')
    app  = fields.ForeignKey(AppResource,'app',full=True)
    class Meta:
        queryset = AppInstalled.objects.filter(redeemed=True)
        resource_name = 'appHistory'

        always_return_data = True 
        filtering = {
            'user': ALL_WITH_RELATIONS,
            }
            
        authorization = DjangoAuthorization()
        authentication = OAuth20Authentication()            
        list_allowed_methods = ['get']            


 
# Recharge Resource 

class RechargeResource(ModelResource):
    user = fields.ForeignKey(UserResource,'user')

    class Meta:
    
        limit=100
        queryset = Recharge.objects.all()
        resource_name = 'recharge'
        filtering = {
            'user': ALL_WITH_RELATIONS,
            }  
        authorization = DjangoAuthorization()
        authentication = OAuth20Authentication()                 
        list_allowed_methods = ['get']            


    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/makeRecharge%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('makeRecharge'), name="api_makeRecharge"),


        ]



    def makeRecharge(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        self.is_authenticated(request)

        try:

           data = self.deserialize(request, request.body, format = request.META.get('CONTENT_TYPE', 'application/json'))

           username = data.get('username', '')
           amount = data.get('amount', '')
           phoneNumber = data.get('phoneNumber', '')
           provider=data.get('provider','')
           coin = data.get('coin', '')
           deviceID = data.get('deviceID', '')

           rechargeAmount=amount

           user=User.objects.get(username=username)

           userProfile=UserProfile.objects.get(user=user)

           wallet=Wallet.objects.get(user=userProfile)


           dor=datetime.now().date()

           if Recharge.objects.filter(Q(SUCCESS=True) | Q(PENDING=True),user=user,date__year=dor.year,date__month=dor.month,date__day=dor.day).count() >= 2:

               return self.create_response(request, {"status":False,"massage":'008'}, HttpUnauthorized )           	

#008  ->   can Not Recharge more than Two Time





           try:
               C=Coins.objects.get(rupee=amount,is_active=True)
               d=Coins.objects.get(coin=coin,is_active=True)                
           except Exception, e:

               email_subject = 'App Pick Hack'
               email_body = user.username + " try to access Your Recharge api using Hack "
               send_mail(email_subject, email_body, settings.EMAIL_HOST_USER,[settings.ADITYA_EMAIL], fail_silently=False)

           
               return self.create_response(request, {"status":False,"massage":'007'}, HttpUnauthorized )



#007  ->   amount not valid




           dd=str(userProfile.deviceID.encode('ascii','ignore')) == str(deviceID)


           if dd is False:
               return self.create_response(request, {"status":False,"massage":'003'}, HttpUnauthorized )
                            
#003  ->   This is not your device



           if int(coin) <= 9:
               return self.create_response(request, {"status":False,"massage":'000'}, HttpUnauthorized )
#000  ->   min . recharge ammount must be 10

           if wallet.coin <= 9:
               return self.create_response(request, {"status":False,"massage":'001'}, HttpUnauthorized )
#001 -> recharge not avilabel bellow 10 Rs

           if wallet.coin-coin < 0:
                return self.create_response(request, {"status":False,"massage":'002'}, HttpUnauthorized )
#002 insufficent ammount in wallet

           recharge=Recharge.objects.create(user=user,rupee=amount,phoneNumber=phoneNumber,provider=provider)
           recharge.save()



#           email_subject = 'New Request for Recharge from Pic-app User'
#           email_body = "User Name : "+str(username)+" \nMobile No : "+str(phoneNumber)+" \nProvider : "+str(provider)+"\nAmount : "+str(amount)
#           send_mail(email_subject, email_body, settings.EMAIL_HOST_USER,["lalitsingh@innotical.com"], fail_silently=False)






       
           
           r=requests.get("http://jolo.in/api/recharge_advance.php?mode=1&key="+str(settings.JOLO_ID)+"&operator="+str(provider)+"&service="+str(phoneNumber)+"&amount="+str(amount)+"&orderid="+str(recharge.id))
           rs=[]
           pattern =re.compile("^\s+|\s*,\s*|\s+$")

           for x in pattern.split(r.text):
           	    rs.append(x)

           recharge.orderID=rs[0]


           if rs[1] == "SUCCESS":
               recharge.SUCCESS=True
               recharge.status=True
               wallet.coin=wallet.coin-coin
               wallet.save()
               
               recharge.save()                             
               return self.create_response(request, {"status":True,"massage":'004'})                
               
# 004-> Rechareg Success               

           elif rs[1] == "FAILED":
               recharge.FAILED=True  
                            
               recharge.save()                             
               return self.create_response(request, {"status":False,"massage":'006'}) 
                             
# 006-> Rechareg Fail          
                            

           elif rs[1] == "PENDING":
               recharge.PENDING=True
               #recharge.status=True
               wallet.coin=wallet.coin-coin
               wallet.save()
               
               recharge.save()                             
               return self.create_response(request, {"status":True,"massage":'005'}) 
                                                           
               
# 005-> Rechareg Pending   




        except Exception,e:
           return self.create_response(request, {"status":False,"massage":e}, HttpUnauthorized )



  
  
  
  
  
  
 
           
           
# Test Model for password and username

class MyResource(ModelResource):

    class Meta:
        queryset = User.objects.all()
        resource_name = 'my'
        authorization = Authorization()      
        always_return_data = True 
        excludes = [ 'password', 'is_active', 'is_staff', 'is_superuser']
        #list_allowed_methods = ['post']
        #detail_allowed_methods = ['get']
        
        authorization = DjangoAuthorization()
        authentication = OAuth20Authentication()        
        

        filtering = {
            'username': ALL,
            'id' : ALL,
         }
           













class RegisterResource(Resource):

    class Meta:
        resource_name = 'register'

    def prepend_urls(self):
        return [

            url(r"^(?P<resource_name>%s)/signup%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('signup'), name="api_signup"),

            url(r"^(?P<resource_name>%s)/login%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('login'), name="api_login"),
                
                
 
            url(r"^(?P<resource_name>%s)/forgotPassword%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('forgotPassword'), name="forgotPassword"),
 
            url(r"^(?P<resource_name>%s)/forgotUsername%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('forgotUsername'), name="forgotUsername"), 
               

        ]



    def signup(self, request, **kwargs):
        self.method_check(request, allowed=['post'])

        data = self.deserialize(request, request.body, format = request.META.get('CONTENT_TYPE', 'application/json'))

        try:
           username = data.get('username', '')
           first_name = data.get('first_name', '')
           last_name = data.get('last_name', '')
           deviceID = data.get('deviceID', '')
      
           
           phoneNumber = data.get('phoneNumber', '')
           referUser = data.get('referUser', '')
           email = data.get('email', '')
           password = data.get('password', '')
           country = data.get('country', '')
#           admin_token = data.get('admin_token', '')




           #print(username+'username')
           #print(first_name+'first_name')
           #print(last_name+'last_name')
           #print(deviceID+'device_id')
           #print(phoneNumber+'phoneNumber')
           #print(referUser+':::: referuser')
           #print(email+'email')
           #print(password+'password')
           #print(country+'country')
           
           
# Checking Admin Token
#           token= admin_token == settings.ADMIN_TOKEN

#           if token is False:
#               app_pick_mail('App-pick Hack','some one suspicious try to register',settings.ADITYA_EMAIL)
#               return self.create_response(request, {'success': False,'reason':'003' }, HttpUnauthorized )            
# "003" -> suspicious           
           

           # checking for unique user

           if User.objects.filter(username=username).exists():
               return self.create_response(request, {'success': False,'reason':'000'}, HttpUnauthorized )
# 000 ->  "Username must be unique."
           #checking for unique email
           if User.objects.filter(email=email).exists():
               return self.create_response(request, {'success': False,'reason':'001'}, HttpUnauthorized )
#001 ->  "Email must be unique."

           #checking for unique device id
           if UserProfile.objects.filter(deviceID=deviceID).exists():
               return self.create_response(request, {'success': False,'reason':'002'}, HttpUnauthorized )
#002 -> "Device id  must be unique."

           # when refer User none or not


           if len(referUser) == 0:


               userObj=User.objects.create(username=username,email=email,first_name=first_name,last_name=last_name)
               userObj.save()

               userObj.set_password(password)
               userObj.save()

               userProfile=UserProfile.objects.create(user=userObj,country=country,phoneNumber=phoneNumber,deviceID=deviceID)
               userProfile.save()

               c=Client(user=userObj, name="mysite client", client_type=1, url="http://app-pick.com/")
               c.save()
               
               
               
               walletObj=Wallet.objects.create(user=UserProfile.objects.get(user=userObj),coin=0)
               walletObj.save()              
               
               
               a=AccessToken.objects.create(user=userObj,client=c)
        
               a.token=password_generator_upper()
               a.expires=datetime.now()
               a.scope=1

               a.save()
               
               
               



               return self.create_response(request, {'success': True,'reason': None})
           else:

               if User.objects.filter(username=referUser).exists():

                   userObj=User.objects.create(username=username,email=email,first_name=first_name,last_name=last_name)
                   userObj.save()

                   userObj.set_password(password)
                   userObj.save()

                   referUserobj=User.objects.get(username=referUser)

                   userProfile=UserProfile.objects.create(user=userObj,country=country,phoneNumber=phoneNumber,deviceID=deviceID,referBy=referUserobj)
                   userProfile.save()


                   c=Client(user=userObj, name="mysite client", client_type=1, url="http://app-pick.com/")
                   c.save()
                   
                   
                   
                   a=AccessToken.objects.create(user=userObj,client=c)
        
                   a.token=password_generator_upper()
                   a.expires=datetime.now()
                   a.scope=1

                   a.save()
                   
                    #initialize User Wallet With 0 coin
                   walletObj=Wallet.objects.create(user=UserProfile.objects.get(user=userObj),coin=0)
                   walletObj.save()
                   
                   
                    #initialize Refer User Wallet 
                   walletObj=Wallet.objects.get(user=UserProfile.objects.get(user=referUserobj))
                   walletObj.coin=walletObj.coin+REFER_COIN
                   walletObj.save()
                   
                   
                   
                   #referObj=Refer.objects.create(from_user=referUserobj,to_user=userObj)
                   #referObj.save()
                   
                   
                   
                   
                   
                   
                   
       


                   return self.create_response(request, {'success': True,'reason': None})

               else:
                   return self.create_response(request, {'success': False,'reason':'003'}, HttpUnauthorized )
# 003 -> "refer user is not exist"




        except Exception,e:
            return self.create_response(request, {'success': False,'reason': e}, HttpUnauthorized )



    def login(self, request, **kwargs):
        self.method_check(request, allowed=['post'])

        data = self.deserialize(request, request.body, format = request.META.get('CONTENT_TYPE', 'application/json'))

        username = data.get('username', '')
        password = data.get('password', '')
        deviceID = data.get('deviceID', '')
        gcmID = data.get('gcmID', '')     
           
           
        #print deviceID
        #print username
        #print password

        try:

            user = authenticate(username=username, password=password)
            if user:
                if user.is_active:
                    login(request, user)


                    userObj=User.objects.get(username=username)


                # update device id

                    userProfile=UserProfile.objects.get(user=userObj)
                    userProfile.deviceID=deviceID
                    userProfile.gcmID=gcmID                    
                    userProfile.save()

                #delete access token

                    for access in AccessToken.objects.filter(user=userObj):
                        access.token=password_generator_upper()
                        access.expires=datetime.now()
                        access.save()


                    c=Client.objects.get(user=userObj)


                    return self.create_response(request, {'client_id':c.client_id,'client_secret':c.client_secret,'success': True,'reason': None,})
                else:
                    return self.create_response(request, {'client_id':None,'client_secret':None,'success': False,'reason':'000'}, HttpUnauthorized)
#000 -> account disabled                    
                    
                    
            else:
                return self.create_response(request, {'client_id':None,'client_secret':None,'success': False,'reason':'001',}, HttpUnauthorized )
#001 -> 'incorrect username or  password '

        except Exception,e:
            return self.create_response(request, {'client_id':None,'client_secret':None,'success': False,'reason': e,}, HttpUnauthorized )
            
    
    def forgotPassword(self, request, **kwargs):
        self.method_check(request, allowed=['post'])


        try:
           data = self.deserialize(request, request.body, format = request.META.get('CONTENT_TYPE', 'application/json'))
           email = data.get('email', '')
           deviceID = data.get('deviceID', '')


           if User.objects.filter(email=email).exists() is False:
                return self.create_response(request, {"status":False,"massage":'000'}, HttpUnauthorized)
# 000 ->"This is email not associate with any Account."                

           if UserProfile.objects.filter(deviceID=deviceID).exists() is False:
                return self.create_response(request, {"status":False,"massage":'001'}, HttpUnauthorized)

#001 ->"Your device is not associate with any Account.."


           userObj=User.objects.get(email=email)

           if userObj.is_active:
               print("user is active")

               password=password_generator_upper()
               userObj.set_password(password)
               userObj.save()


               email_subject = 'AppPick Forgot Password'
               email_body = "Your new Password is : "+password+"\nIt is highly recomended that you change your password after login. \n\n                                        Happy App Picking \n Regards \n AppPick Team"
               send_mail(email_subject, email_body, settings.EMAIL_HOST_USER,[userObj.email], fail_silently=False)

               return self.create_response(request, {"status":True,"massage":"Please check your email to Reset your password."})



           else:
               #print("else")
               return self.create_response(request, {"status":False,"massage":"This User is not Active."}, HttpUnauthorized )


        except Exception,e:

            return self.create_response(request, {"status":False,"massage":e}, HttpUnauthorized )




    def forgotUsername(self, request, **kwargs):
        self.method_check(request, allowed=['post'])


        try:
           data = self.deserialize(request, request.body, format = request.META.get('CONTENT_TYPE', 'application/json'))
           email = data.get('email', '')
           deviceID = data.get('deviceID', '')


           if User.objects.filter(email=email).exists() is False:
                return self.create_response(request, {"status":False,"massage":'000'}, HttpUnauthorized)
# 000 ->"This is email not associate with any Account."                

           if UserProfile.objects.filter(deviceID=deviceID).exists() is False:
                return self.create_response(request, {"status":False,"massage":'001'}, HttpUnauthorized)

#001 ->"Your device is not associate with any Account.."


           userObj=User.objects.get(email=email)

           if userObj.is_active:
               print("user is active")




               email_subject = 'AppPick Forgot Username'
               email_body = "Your Username is : "+userObj.username+"\n \n                           Happy App Picking \n Regards \n AppPick Team"
               send_mail(email_subject, email_body, settings.EMAIL_HOST_USER,[userObj.email], fail_silently=False)

               return self.create_response(request, {"status":True,"massage":"Please check your email to Get Username."})



           else:
               #print("else")
               return self.create_response(request, {"status":False,"massage":"This User is not Active."}, HttpUnauthorized )


        except Exception,e:

            return self.create_response(request, {"status":False,"massage":e}, HttpUnauthorized )









    
# History


class HistoryResource(Resource):

    class Meta:
        resource_name = 'history'
        authorization = DjangoAuthorization()
        authentication = OAuth20Authentication()          
        
        

    def prepend_urls(self):
        return [

            url(r"^(?P<resource_name>%s)/visits%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('visits'), name="api_visits"),


        ]


    
    
    

    def visits(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        self.is_authenticated(request)

        try:


           data = self.deserialize(request, request.body, format = request.META.get('CONTENT_TYPE', 'application/json'))

           username = data.get('username', '')
           package = data.get('package', '')
           countryCode = data.get('countryCode', '')
           deviceID = data.get('deviceID', '')


 


           #checking for app

           if App.objects.filter(package=package).exists():

               app=App.objects.get(package=package)
               
    
               aa=AppVisits.objects.create(app=app,date=datetime.now().date(),countryCode=countryCode,deviceID=deviceID)
               aa.save()
               
               return self.create_response(request, {"status":True,"massage":"Done"})           
                  
   
               
               
           #checking for GAME

           elif Game.objects.filter(package=package).exists():

               game=Game.objects.get(package=package)
               
               
               gg=GameVisits.objects.create(game=game,date=datetime.now().date(),countryCode=countryCode,deviceID=deviceID)
               gg.save()
               
                    
               return self.create_response(request, {"status":True,"massage":"Done"})             
  
           else:
           
          
                return self.create_response(request, {"status":False,"massage":"Error"}, HttpUnauthorized ) 
 
 
        except Exception,e:

            return self.create_response(request, {"status":False,"massage":e}, HttpUnauthorized )
    
    
    
# Gift


class GiftResource(ModelResource):
    class Meta:
        queryset = Gift.objects.filter(is_active=True)
        resource_name = 'gift'
        
        authorization = DjangoAuthorization()
        authentication = OAuth20Authentication()        
        list_allowed_methods = ['get']       
        excludes = ['is_active',]
      
    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/getGift%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('getGift'), name="api_getGift"),

        ]



    def getGift(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        self.is_authenticated(request)
        data = self.deserialize(request, request.body, format = request.META.get('CONTENT_TYPE', 'application/json'))

        username = data.get('username', '')
        id = data.get('id', '')

        try:


        	userObj=User.objects.get(username=username)

        	user=UserProfile.objects.get(user=userObj)

        	wallet=Wallet.objects.get(user=user)


        	gift=Gift.objects.get(id=int(id))




        	if int(wallet.coin) < int(gift.coin):
        		return self.create_response(request, {"status":False,"resion":"000"},HttpUnauthorized)
# 000 -> you have insufficient coin.       		
        	else:
        		giftRedeem=GiftRedeem.objects.create(user=user,gift=gift)
        		giftRedeem.save()

        		wallet.coin=int(wallet.coin)-int(gift.coin)
        		wallet.save()

        		return self.create_response(request, {"status":True,"reason":"Done"})





        except Exception,e:
            return self.create_response(request, {"status":False,"reason":e},HttpUnauthorized)
            
            
 
   
# Events


class EventsResource(ModelResource):
    game = fields.ForeignKey(GameResource,'game',full=True,null=True)   	
    class Meta:
        queryset = Events.objects.filter(is_active=True)
        resource_name = 'event'
        
        authorization = DjangoAuthorization()
        authentication = OAuth20Authentication()        
        list_allowed_methods = ['get']       
        excludes = ['is_active',]

        filtering = {
            'game': ALL_WITH_RELATIONS,
            }


      
    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/eventRedeem%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('eventRedeem'), name="api_eventRedeem"),

        ]



    def eventRedeem(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        self.is_authenticated(request)
        data = self.deserialize(request, request.body, format = request.META.get('CONTENT_TYPE', 'application/json'))

        username = data.get('username', '')
        event_key = data.get('event_key', '')

        try:
        	
            userObj=User.objects.get(username=username)
            user=UserProfile.objects.get(user=userObj)
            wallet=Wallet.objects.get(user=user)
            event=Events.objects.get(event_key=str(event_key))

            wallet.coin=int(wallet.coin)+int(event.coin)
            wallet.save()      


            return self.create_response(request, {"status":True,"reason":"Done"})





        except Exception,e:
            return self.create_response(request, {"status":False,"reason":e},HttpUnauthorized)


   
            
            
            
# Coins


class CoinsResource(ModelResource):
	
    class Meta:
        queryset = Coins.objects.filter(is_active=True)
        resource_name = 'coins'
        
        authorization = DjangoAuthorization()
        authentication = OAuth20Authentication()        
        list_allowed_methods = ['get']       
        excludes = ['is_active','id',]

  

            
            
            
 
 
#App Version 
            
class AppVersionResource(Resource):

    class Meta:
        resource_name = 'appVersion'

    def prepend_urls(self):
        return [

            url(r"^(?P<resource_name>%s)/check%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('check'), name="api_check"),

        ]



    def check(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        #self.is_authenticated(request)
        data = self.deserialize(request, request.body, format = request.META.get('CONTENT_TYPE', 'application/json'))

        try:
           name = data.get('name', '')
           
           if name == settings.APP_VER:
               return self.create_response(request, {'success': True,'reason': None}) 
           else:   
               return self.create_response(request, {'success': False,'reason': None}) 


        except Exception,e:
            return self.create_response(request, {'success': False,'reason': e}, HttpUnauthorized )




 
#Refer information 
            
class ReferResource(Resource):

    class Meta:
        resource_name = 'refer'
        authorization = DjangoAuthorization()
        authentication = OAuth20Authentication()



    def prepend_urls(self):
        return [

            url(r"^(?P<resource_name>%s)/info%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('info'), name="api_info"),

        ]



    def info(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        self.is_authenticated(request)
        data = self.deserialize(request, request.body, format = request.META.get('CONTENT_TYPE', 'application/json'))

        try:
            coin =REFER_COIN
            return self.create_response(request, {'success': True,'coin': coin,'reason': None})           
    

        except Exception,e:
            return self.create_response(request, {'success': False,'coin': 0,'reason': e}, HttpUnauthorized )



 
def app_pick_mail(email_subject,email_body,email_reciver):
    email_subject = email_subject
    email_body = email_body
    send_mail(email_subject, email_body, settings.EMAIL_HOST_USER,[email_reciver], fail_silently=False)


    
    
  

 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    