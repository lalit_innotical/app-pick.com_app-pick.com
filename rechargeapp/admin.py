from django.contrib import admin
from rechargeapp.models import *

#admin.site.register(UserProfile)
admin.site.register(App)
admin.site.register(AppInstalled)
admin.site.register(GameInstalled)
#admin.site.register(Wallet)
admin.site.register(Game)
admin.site.register(LeaderBoard)
#admin.site.register(Recharge)


admin.site.register(Monthly)
admin.site.register(Weekly)
admin.site.register(Daily)   


admin.site.register(PostBack)
admin.site.register(PostBackOrange)

admin.site.register(AppDownload) 
admin.site.register(GameDownload) 


admin.site.register(AppVisits) 
admin.site.register(GameVisits)


#innotical
admin.site.register(IPTrack)
admin.site.register(IPTrackOne)
admin.site.register(IPTrackTwo)

#Gift

admin.site.register(Gift)
admin.site.register(GiftRedeem)
 
 
#Events
admin.site.register(Events)

#Coins
admin.site.register(Coins)
 
 


class RechargeAdmin(admin.ModelAdmin):


    list_display = ['user','rupee','phoneNumber','date']
    list_filter = ['date']
    search_fields =['rupee','phoneNumber','user__username']
  
admin.site.register(Recharge, RechargeAdmin)


class WalletAdmin(admin.ModelAdmin):


    list_display = ['user']
    list_filter = []
    search_fields =['user__user__username']
  
admin.site.register(Wallet, WalletAdmin)



class UserProfileAdmin(admin.ModelAdmin):


    list_display = ['user','phoneNumber','referBy']
    list_filter = ['date']
    search_fields =['phoneNumber','user__username']
  
admin.site.register(UserProfile, UserProfileAdmin)
 

 
 