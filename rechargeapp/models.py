from django.db import models

# Create your models here.
from django.db import models
from django.contrib.auth.models import User
from datetime import *


# Create your models here.

class UserProfile(models.Model):
    user = models.OneToOneField(User,related_name='main_user')
    phoneNumber = models.CharField(max_length=15)
    country = models.CharField(max_length=50)
    deviceID = models.CharField(max_length=50)
    referBy=models.ForeignKey(User,related_name='refer_user',blank=True,null=True)
    gcmID = models.CharField(max_length=1000,null=True,blank=True)    
    date=models.DateTimeField(default=datetime.now)


    
    def __unicode__(self):
        return self.user.username

class App(models.Model):
    name = models.CharField(max_length=50,blank=False)
    package = models.CharField(max_length=50,blank=False,unique=True)
    icon = models.ImageField(upload_to='app_image',blank=True)
    company = models.CharField(max_length=256,default=0)    
    price = models.IntegerField()
    description=models.TextField()
    is_active=models.BooleanField(default=False)
    is_offer=models.BooleanField(default=False)
    offer_id=models.CharField(max_length=256,default=None,null=True)
    url= models.CharField(max_length=256,default=None,null=True,blank=True)




    def __unicode__(self):
        return self.name





class Game(models.Model):
    name = models.CharField(max_length=50,blank=False)
    package = models.CharField(max_length=50,blank=False,unique=True)
    icon = models.ImageField(upload_to='game_image',blank=True)
    company = models.CharField(max_length=256,default=0)      
    price = models.IntegerField()
    description=models.TextField()
    is_active=models.BooleanField(default=False)
    
    is_monthly_price=models.BooleanField(default=False)
    monthly_first_price=models.IntegerField(default=0)
    monthly_second_price=models.IntegerField(default=0)
    monthly_third_price=models.IntegerField(default=0)


    is_weekly_price=models.BooleanField(default=False)
    weekly_first_price=models.IntegerField(default=0)
    weekly_second_price=models.IntegerField(default=0)
    weekly_third_price=models.IntegerField(default=0)

    
    
    
    

    def __unicode__(self):
        return self.name
        
        
        

class AppInstalled(models.Model):
    user=models.ForeignKey(UserProfile)
    app=models.ForeignKey(App)
    date=models.DateTimeField(default=datetime.now, blank=True)
    redeemed = models.BooleanField(default=False,blank=True)
    
    class Meta:
        unique_together = ("user", "app")

    def __unicode__(self):
        return self.user.user.username
        
        
        
class GameInstalled(models.Model):
    user=models.ForeignKey(UserProfile)
    game=models.ForeignKey(Game)
    date=models.DateTimeField(default=datetime.now, blank=True)
    redeemed = models.BooleanField(default=False)
    
    
    class Meta:
        unique_together = ("user", "game")

    def __unicode__(self):
        return self.user.user.username        
        


class Wallet(models.Model):
    user = models.ForeignKey(UserProfile)
    coin = models.IntegerField(default=0)
    redeemed = models.IntegerField(default=0)

    def __unicode__(self):
        return self.user.user.username
        
        
        


        
class LeaderBoard(models.Model):
    user=models.ForeignKey(User)
    game=models.ForeignKey(Game)
    highScore=models.IntegerField(default=0)
    npt=models.IntegerField(default=0)
    
    class Meta:
        unique_together = ("user", "game")
    
    
    def __unicode__(self):
        return self.user.username      
        
        
class Recharge(models.Model):
    user=models.ForeignKey(User,blank=True,null=True)
    rupee= models.IntegerField()
    provider = models.CharField(max_length=128,blank=True)    
    status=models.BooleanField(default=False)
    phoneNumber = models.CharField(max_length=15)
    date=models.DateTimeField(default=datetime.now)
    orderID= models.CharField(max_length=256,blank=True,null=True)
    is_cron=models.BooleanField(default=False)
    
    SUCCESS=models.BooleanField(default=False)
    FAILED=models.BooleanField(default=False)
    PENDING=models.BooleanField(default=False)


    def __unicode__(self):
        return self.user.username
        
        
             
        
        
        

class Monthly(models.Model):
    user=models.ForeignKey(User)
    game=models.ForeignKey(Game)
    score=models.IntegerField(default=0)
    monthNo=models.IntegerField(default=0)
    date = models.DateField()
    year=models.IntegerField(default=0)    
   

    def __unicode__(self):
        return self.user.username


class Weekly(models.Model):
    user=models.ForeignKey(User)
    game=models.ForeignKey(Game)
    score=models.IntegerField(default=0)
    weekNo=models.IntegerField(default=0)
    date = models.DateField()
    year=models.IntegerField(default=0)     
    month=models.IntegerField(default=0)     
    
    

    def __unicode__(self):
        return self.user.username

class Daily(models.Model):
    user=models.ForeignKey(User)
    game=models.ForeignKey(Game)
    score=models.IntegerField(default=0)
    date = models.DateField()

    def __unicode__(self):
        return self.user.username
        
        
        
        
        
        
 
# no need delete it        
#App History
#####################  
class AppHistory(models.Model):
    app=models.ForeignKey(App)
#    date=models.DateTimeField(default=datetime.now, blank=True)
    date=models.DateField()  
    countryCode=models.CharField(max_length=125,blank=True,null=True)
    totalDownload=models.IntegerField(default=0)
    visits=models.IntegerField(default=0)
    
#    class Meta:
#        unique_together = ("date", "countryCode")

    def __unicode__(self):
        return self.app.name


class GameHistory(models.Model):
    game=models.ForeignKey(Game)
#    date=models.DateTimeField(default=datetime.now, blank=True)
    date=models.DateField()  
    countryCode=models.CharField(max_length=125,blank=True,null=True)
    totalDownload=models.IntegerField(default=0)
    visits=models.IntegerField(default=0)
    
 #   class Meta:
 #       unique_together = ("date", "countryCode")

    def __unicode__(self):
        return self.game.name        
#####################          
 
 
 
# no need delete it
#####################          
class Refer(models.Model):
    from_user = models.ForeignKey(User,related_name='from_user')
    to_user = models.ForeignKey(User,related_name='to_user')
    date=models.DateTimeField(default=datetime.now)

    def __unicode__(self):
        return self.from_user.username        
  
  
#####################    
  
        
          




#  Download

class AppDownload(models.Model):
    app=models.ForeignKey(App)
    date=models.DateField()    
    countryCode=models.CharField(max_length=125,blank=True,null=True)
    deviceID = models.CharField(max_length=50)

    def __unicode__(self):
        return self.app.name

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
class GameDownload(models.Model):
    game=models.ForeignKey(Game)
    date=models.DateField()    
    countryCode=models.CharField(max_length=125,blank=True,null=True)
    deviceID = models.CharField(max_length=50)

    def __unicode__(self):
        return self.game.name



#  Visits

class AppVisits(models.Model):
    app=models.ForeignKey(App)
    date=models.DateField()    
    countryCode=models.CharField(max_length=125,blank=True,null=True)
    deviceID = models.CharField(max_length=50)

    def __unicode__(self):
        return self.app.name

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
class GameVisits(models.Model):
    game=models.ForeignKey(Game)
    date=models.DateField()    
    countryCode=models.CharField(max_length=125,blank=True,null=True)
    deviceID = models.CharField(max_length=50)

    def __unicode__(self):
        return self.game.name


#innotical

#####################  



class IPTrack(models.Model):
    ipAdd=models.CharField(max_length=256)
    date=models.DateTimeField(default=datetime.now)
    newdate=models.DateTimeField(default=datetime.now)

    def __unicode__(self):
            return self.ipAdd        






class IPTrackOne(models.Model):
    ipAdd=models.CharField(max_length=256)
    date=models.DateTimeField(default=datetime.now)


    def __unicode__(self):
            return self.ipAdd        
        

class IPTrackTwo(models.Model):
    ipAdd=models.CharField(max_length=256)
    date=models.DateTimeField(default=datetime.now)


    def __unicode__(self):
            return self.ipAdd        
        
#####################  






#  Gift

class Gift(models.Model):
    name = models.CharField(max_length=50,blank=False)
    icon = models.ImageField(upload_to='Gift_image',blank=True)
    coin = models.IntegerField()
    description=models.TextField()
    is_active=models.BooleanField(default=False)


    def __unicode__(self):
        return self.name



class GiftRedeem(models.Model):
    user = models.ForeignKey(UserProfile)
    gift = models.ForeignKey(Gift)    
    status=models.BooleanField(default=False)
    date=models.DateTimeField(default=datetime.now)

    def __unicode__(self):
        return self.user.user.username


# Events for Game 
class Events(models.Model):
    game = models.ForeignKey(Game)
    name = models.CharField(max_length=256,blank=False)
    task = models.CharField(max_length=500,blank=False)    
    coin = models.IntegerField()
    description=models.TextField()
    event_key = models.CharField(max_length=250,unique=True)    
    is_active=models.BooleanField(default=False)


    def __unicode__(self):
        return self.game.name


# Coins
class Coins(models.Model):   
    coin = models.IntegerField()
    rupee = models.IntegerField()
    is_active=models.BooleanField(default=False)


    def __unicode__(self):
        return str(self.coin)



  
class PostBack(models.Model):
    gee_unique = models.IntegerField(blank=True,default=0,null=True)
    gee_idapp = models.IntegerField(blank=True,default=0,null=True)
    gee_app = models.CharField(max_length=500,blank=True,default=None,null=True)
    gee_ppi= models.FloatField(default=0.0,blank=True,null=True)
    gee_currency=models.CharField(max_length=500,blank=True,default=None,null=True)   
    gee_country=models.CharField(max_length=500,blank=True,default=None,null=True)
    gee_lang=models.CharField(max_length=256,blank=True,default=None,null=True)    
    gee_iddevice = models.IntegerField(blank=True,default=0)
    gee_device=models.CharField(max_length=500,blank=True,default=None,null=True)
    gee_click = models.IntegerField(blank=True,default=0,null=True)
    gee_installation = models.IntegerField(blank=True,default=0,null=True)
    is_check=models.BooleanField(default=False)

    def __unicode__(self):
        return str(self.gee_unique)


class PostBackOrange(models.Model):
    offer_id= models.CharField(max_length=500,blank=True,default=None,null=True)
    mac_address = models.CharField(max_length=500,blank=True,default=None,null=True)
    open_udid = models.CharField(max_length=500,blank=True,default=None,null=True)
    ios_ifa = models.CharField(max_length=500,blank=True,default=None,null=True)
    android_id = models.CharField(max_length=500,blank=True,default=None,null=True)
    google_aid = models.CharField(max_length=500,blank=True,default=None,null=True)
    unique_id = models.CharField(max_length=500,blank=True,default=None,null=True)
    payout = models.CharField(max_length=500,blank=True,default=None,null=True)
    sub_id = models.CharField(max_length=500,blank=True,default=None,null=True)
    phone_model_id = models.CharField(max_length=500,blank=True,default=None,null=True)
    is_check=models.BooleanField(default=False)



    def __unicode__(self):
        return str(self.offer_id)





  
  
        
                    