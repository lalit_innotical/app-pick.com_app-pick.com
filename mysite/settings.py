import os

BASE_DIR = os.path.dirname(os.path.dirname(__file__))
STATIC_PATH = os.path.join(BASE_DIR, 'static')
MEDIA_ROOT = os.path.join(BASE_DIR,'media')
MEDIA_URL = '/media/'

TEMPLATE_PATH = '/home/apppick/public_html/mysite/templates'



# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'ns6#fmktekb%nh4t=&g&+4b2nwd3cq)ud98ygy6)kqfbpy9oh7'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

TEMPLATE_DEBUG =False



ALLOWED_HOSTS = [
    '.app-pick.com', # Allow domain and subdomains
   
]


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'debug_toolbar',  
    'south',  
    'provider',
    'provider.oauth2',    
    'tastypie',  
    'rechargeapp',
    'django_crontab', 
    'corsheaders',       
    
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'corsheaders.middleware.CorsMiddleware',    
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'debug_toolbar.middleware.DebugToolbarMiddleware',    
)


USE_ETAGS = True
CORS_ORIGIN_ALLOW_ALL = True


DEBUG_TOOLBAR_PATCH_SETTINGS = False

INTERNAL_IPS=('122.177.5.200',)

ROOT_URLCONF = 'mysite.urls'

WSGI_APPLICATION = 'mysite.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
            'NAME': 'apppicdb',                      # Or path to database file if using sqlite3.
            # The following settings are not used with sqlite3:
            'USER': 'apppic',
            'PASSWORD': 'mohan9898',
            'HOST': '',                      # Empty for localhost through domain sockets or           '127.0.0.1' for localhost through TCP.
            'PORT': '',                      # Set to empty string for default.
        }
    }

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/





#email details

EMAIL_USE_TLS = True
EMAIL_HOST ='mail.app-pick.com'
EMAIL_HOST_USER ='admin@app-pick.com'
EMAIL_HOST_PASSWORD ='lalit9898'
EMAIL_PORT = 25

KEY_ID="ideaFoodPassword"

CLIENT_EMAIL="lalitsingh@innotical.com"
ADITYA_EMAIL="aditya@innotical.com"

#email details

#jolo clientid

JOLO_ID="cburi"
#JOLO_ID="cburiedseedgames307"



JOLO_USER_ID="buriedseedgames"

APP_VER="1.0.1"

#Admin
ADMIN_TOKEN="4Qf0VNWOJqnO8ge03IHwAg5555%&gws_rd"


#gcm ID

GCM_ID="AIzaSyD3aLXo8izCh1ZLqBdBCHP8nSUaWrwedGE"

CRONJOBS = [
    #('*/5 * * * *', 'mysite.cron.my_scheduled_job'),
    #('0 23 28 * *', 'mysite.cron.my_scheduled_month_job'),
    #('0 23 * * 6', 'mysite.cron.my_scheduled_week_job'),
    #('*/10 * * * *', 'mysite.cron.my_scheduled_offer_job'),                  
]






LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True





# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_ROOT=STATIC_PATH


STATIC_URL = '/static/'

PREPEND_WWW = True

TEMPLATE_DIRS = [
    TEMPLATE_PATH,
]
