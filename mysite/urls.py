from django.conf.urls import patterns, include, url
from django.conf import settings
from django.contrib import admin
from django.views.generic import TemplateView

from rechargeapp.views import *

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    
    url(r'^$', TemplateView.as_view(template_name="index.html")),
    
    
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    
    url(r'^recharge/', include('rechargeapp.urls')),
    url(r'^oauth2/', include('provider.oauth2.urls', namespace = 'oauth2')),  
    
    
    url(r'^terms/',  TemplateView.as_view(template_name="terms.html")),
    url(r'^privacyPolicy/',  TemplateView.as_view(template_name="privacy-policy.html")),  
    url(r'^contactUS/',  TemplateView.as_view(template_name="ContactUs.html")), 


    url(r'^laff/apple/', LaffAppleView.as_view()),   
    url(r'^laff/orange/', LaffOrangeView.as_view()),     
    
    
)




if settings.DEBUG:
    import debug_toolbar
    urlpatterns += patterns('',
        url(r'^__debug__/', include(debug_toolbar.urls)),
    )
 
 
    
#if True:
#    urlpatterns += patterns(
#        'django.views.static',
#        (r'media/(?P<path>.*)',
#        'serve',
#        {'document_root': settings.MEDIA_ROOT}), )

    
    
    